package com.kpi;


import java.util.Scanner;

/**
 * Created by Yuriy on 26.04.2016.
 */
public class Clothes {
    private String Type;
    private String Country;
    private String Brand;
    private int Year;
    private double Price;
    public Scanner Mat = new Scanner(System.in);
    //Constructor
    public Clothes(String T, String C, String B, int Y, double P){
        System.out.println("Write your own clothes to class:");
        System.out.println("1.Type of clothing");
        T = Mat.nextLine();
        SetType(T);
        System.out.println("2.Made in country");
        C = Mat.nextLine();
        SetCountry(C);
        System.out.println("3.Brand of this clothing");
        B = Mat.nextLine();
        SetBrand(B);
        System.out.println("4.Year of production");
        Y = Mat.nextInt();
        SetYear(Y);
        System.out.println("5.Price of it");
        P =  Mat.nextDouble();
        SetPrice(P);
    }

    //Setters
    public void SetType(String T){
        this.Type = T;
    }
    public void SetCountry(String C){
        this.Country = C;
    }
    public void SetBrand(String B){
        this.Brand = B;
    }
    public void SetYear(int Y){
        this.Year = Y;
    }
    public void SetPrice(double P){
        this.Price = P;
    }

    //Getters
    public String GetType(){
        return Type;
    }
    public String GetCountry(){
        return Country;
    }
    public String GetBrand(){
        return Brand;
    }
    public int GetYear(){
        return Year;
    }
    public double GetPrice(){
        return Price;
    }
}
