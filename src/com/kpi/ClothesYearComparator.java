package com.kpi;

import java.util.Comparator;

/**
 * Created by Yuriy on 26.04.2016.
 */
public class ClothesYearComparator implements Comparator<Clothes> {
    public int compare(Clothes a, Clothes b){
        if(a.GetYear() > b.GetYear())
            return 1;
        else if(a.GetYear() < b.GetYear())
            return -1;
        else
            return 0;
    }
}
