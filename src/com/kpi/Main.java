package com.kpi;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int z = 5208;
        double C11 = z % 11;
        System.out.println("C11="+C11);
        //C11=Визначити клас одяг, який складається як мінімум з 5-и полів.

        Scanner cl = new Scanner (System.in);
        String Ty = "";
        String Co = "";
        String Br = "";
        int Ye = 0;
        double Pr = 0.0;
        System.out.println("Write a number of clothes you want:");
        int n = cl.nextInt();
        int i = 0;
        Clothes[] arr = new Clothes[n];
        while (i < n){
            Clothes MyClothes = new Clothes(Ty, Co, Br, Ye, Pr);
            arr[i] = MyClothes;
            i++;
        }

        System.out.println("You entered this elements:");
        for (i = 0; i < n; i++){
            System.out.println(arr[i].GetType()+" "+arr[i].GetCountry()+" "+arr[i].GetBrand()+" "+arr[i].GetYear()+" "+arr[i].GetPrice());
        }

        System.out.println(" ");
        System.out.println("Sorted by Country and Year will be:");
        Comparator<Clothes> Clo = new ClothesCountryComparator().thenComparing(Collections.reverseOrder(new ClothesYearComparator()));
        Arrays.sort(arr, Clo);
        for (i = 0; i < n; i++){
            System.out.println(arr[i].GetType()+" "+arr[i].GetCountry()+" "+arr[i].GetBrand()+" "+arr[i].GetYear()+" "+arr[i].GetPrice());
        }
    }
}
