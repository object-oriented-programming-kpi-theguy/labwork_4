package com.kpi;

import java.util.Comparator;

/**
 * Created by Yuriy on 26.04.2016.
 */
public class ClothesCountryComparator implements Comparator<Clothes> {
    public int compare(Clothes a, Clothes b){
        return a.GetCountry().compareTo(b.GetCountry());
    }
}
